# My Docker Test

Deployment: Docker-Compose, Dockerfile

Front-end: ReactJS/Typescript/Webpack/Express/Redux/Axios/NGINX

Back-end: NodeJS/TypeScript/MongoDB/Express

## Quick Start:

* Navigate to play-with-docker.com

* add a new docker instance.

* Clone My Docker Test from GIT.
```bash
git clone https://gitlab.com/dfleming/my-docker-test.git
```

* Change directory to my-docker-test
```bash
cd my-docker-test
```

* Run as services in the background
```bash
docker-compose up -d
```

## Summary:

* NodeJS back-end.  
* Uses Express framework for URI routing.  
* express.Router class used to modularize end points.
* Written in Typescript which is compiled before server start into Javascript in the dist folder.
* Connects to a MongoDB instance.
* NodeJS and MongoDB deployed to docker via docker-compose
* Redux is used for predictable state container
* Axios is used for AJAX requests
* Webpack used for module bundler
* JS Inline Styling
* NGINX reverse proxy server
* SSL

## Notes:

Currently compiles typescript in the container.  I'm thinking production version should only include distribution files which have been pre-compiled to minimize footprint.

## TODO:

* MongoDB initialization strategy

* MongoDB CRUD Operations
    * https://docs.mongodb.com/manual/crud/

* MongoDB Cluster - Replication and Sharding
    * https://docs.mongodb.com/manual/replication/
    * https://docs.mongodb.com/manual/sharding/

* MongoDB Authentication - db.auth/inline url user:pass@
    * https://docs.mongodb.com/manual/reference/command/#user-management-commands
    * https://docs.mongodb.com/manual/core/security-users/#sharding-security

* MongoDB Storage -> Volume (Docker managed or data directory on host system)
    * https://docs.docker.com/v1.10/engine/userguide/containers/dockervolumes/

* Docker Swarm
    * https://docs.docker.com/engine/swarm/#feature-highlights

* Docker Secrets
    * https://docs.docker.com/engine/swarm/secrets/


## Nifty docker commands:
* bash in image
```bash
docker run -it <image_name> bash
```

* bash in image and change entrypoint
```bash
docker run -it --entrypoint=/bin/bash <container_name>
```

* remove dangling images
```bash
docker rmi $(docker images --quiet --filter "dangling=true")
```

* bring down containers, pull newest code, rebuild (using cache), and bring back up detached
```bash
docker-compose down && git pull && docker-compose build && docker-compose up -d
```

*

http://www.seleniumtests.com/2015/12/distributed-testing-with-jmeter-when.html
http://www.testautomationguru.com/jmeter-distributed-load-testing-using-docker/