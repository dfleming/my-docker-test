"use strict";
const express = require('express');

const app = express();
const port = 9000;
app.listen(port, function () {
    console.log('Server running on port: ' + port + '/');
});

//make dist bundle visible on root
app.use(express.static('dist'));
