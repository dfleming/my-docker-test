var path = require('path');
var rules = require("./rules");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');

module.exports = function () {
    return {
        entry: {
            app: './src/index.tsx',
            vendor: ["react", "react-dom"]
        },
        resolve: {
            extensions: ['.ts', '.tsx', 'jsx', '.js', '.json', '.css'],
            modules: [
                'node_modules',
                path.resolve(__dirname, "src"),
                path.resolve(__dirname, "public")
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './public/index.html',
                inject: 'body',
                hash: true
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor'
            })
        ],
        module: {
            rules: rules
        }
    };
}