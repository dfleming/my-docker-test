var webpack = require('webpack');
var path = require('path');
var webpackMerge = require('webpack-merge');
var commonConfig = require('./base.config');

var proxy = [{
	context: ['/'],
	changeOrigin: true,
	target: "http://localhost:3000",
	secure: false
}];

module.exports = function (env) {
	return webpackMerge(commonConfig(), {
		devtool: "inline-eval-cheap-source-map",
		output: {
			path: path.join(__dirname, '/dev'),
			filename: '[name].bundle.js'
		},
		devServer: {
			host: "localhost",
			port: 9000,
			https: false,
			proxy: proxy
		}
	})
};