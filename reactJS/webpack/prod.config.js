var path = require('path');
var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var commonConfig = require('./base.config.js');

module.exports = function (env) {
    return webpackMerge(commonConfig(), {
        devtool: "cheap-module-source-map",
        output: {
            path: path.join(__dirname, '../dist'),
            filename: '[name].bundle.min.js'
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }),
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            })
            // ,
            // new webpack.optimize.UglifyJsPlugin({
            //     beautify: false,
            //     mangle: {
            //         screw_ie8: true,
            //         keep_fnames: true
            //     },
            //     compress: {
            //         screw_ie8: true
            //     },
            //     comments: false
            // })
        ]
    })
}
