module.exports = [
    {
        test: /\.js(x?)$/,
        exclude: [/node_modules/],
        loader: 'babel-loader',
        query: { compact: false, presets: ['react', 'es2015'] }
    },
    {
        test: /\.ts(x?)$/,
        include: [/src/, /public/, /node_modules/],
        use: 'ts-loader'
    },
    {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
    },
    {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
    },
    {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
    },
    {
        test: /\.html$/,
        exclude: /node_modules/,
        use: 'raw-loader'
    },
    {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000&mimetype=application/font-woff'
    },
    {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'file-loader'
    },
    {
        test: /\.jpg$/,
        exclude: /node_modules/,
        use: 'file-loader'
    },
    {
        test: /\.png$/,
        exclude: /node_modules/,        
        use: 'url-loader'
    }
];