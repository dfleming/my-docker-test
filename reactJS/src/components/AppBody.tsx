import * as React from "react";
import { ParentComponent } from "./stateLifting/ParentComponent";
import { IPAddressWidget} from "./widgets/IPAddressWidget";
import { NodeJSWidget } from "./widgets/NodeJSWidget";
import { ChatWidget } from "./widgets/chat/ChatWidget";
import { NewsWidget } from "./widgets/NewsWidget";
export interface IProps {};
export interface IState {};

export class AppBody extends React.Component<IProps, IState> {
    render() {
        return <div>
          <NodeJSWidget/>
          <IPAddressWidget/>
          <ParentComponent propValue="Some Value"/>
          <ChatWidget/>
          <NewsWidget/>
        </div>;
    }
}
