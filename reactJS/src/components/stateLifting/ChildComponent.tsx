import * as React from "react";
import { widgetStyle } from '../../style'

export interface IProps {propValue: string, onPropValueChange:(newValue:string)=>any};
export interface IState {};

export class ChildComponent extends React.Component<IProps, IState> {
  
    constructor(props:IProps) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
    }
  
    handleChange(e:React.FormEvent<HTMLInputElement>){
      //propagate the value upwards
      this.props.onPropValueChange(e.currentTarget.value);
    }
  
    render() {
        return <div style={widgetStyle}>
          <span>State Lifting Widget(Child):</span>
          <br/>
          <p>Child Prop Value: {this.props.propValue}!</p>
          Child Input: <input value={this.props.propValue} onChange={this.handleChange}/>
          </div>;
    }
}