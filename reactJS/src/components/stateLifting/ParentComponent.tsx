import * as React from "react";

import { ChildComponent } from "./ChildComponent";
import { widgetStyle } from '../../style'

export interface IProps {propValue: string};
export interface IState {myValue: string};

export class ParentComponent extends React.Component<IProps, IState> {
    constructor(props:IProps){
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.handleChildValueChange = this.handleChildValueChange.bind(this);
      this.state = {myValue:props.propValue};
    }
  
    handleChange(e:React.FormEvent<HTMLInputElement>){
      this.setState({myValue: e.currentTarget.value});
    }
  
    handleChildValueChange(childValue: string){
      this.setState({myValue: childValue});
    }
  
    render() {
        const myValue = this.state.myValue;
        return <div style={widgetStyle}>
          <span>State Lifting Widget(Parent):</span>
          <br/>
          <p>Parent Prop Value: {this.state.myValue}</p>
          Parent Input: <input value={myValue} onChange={this.handleChange}/>
          
          <ChildComponent propValue={this.state.myValue} onPropValueChange={this.handleChildValueChange}/>
          </div>;
    }
}