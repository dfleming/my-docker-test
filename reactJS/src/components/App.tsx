import * as React from "react";
import { AppFooter } from "./AppFooter";
import { AppBody } from "./AppBody";

export interface IProps {};
export interface IState {};

export class App extends React.Component<IProps, IState> {
    render() {
        var currentDateTime = new Date();
        return <div>
                <AppBody/>
                <AppFooter dateTime={currentDateTime} />
              </div>
        
         ;
    }
}