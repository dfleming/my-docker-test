import * as React from "react";
import {widgetStyle} from '../style'

export interface IProps {};
export interface IState {currentDate: Date, timerID: number};

export class Clock extends React.Component<IProps, IState> {
  
    constructor(props:IProps){
        super(props);
        this.state = {currentDate: new Date(), timerID: -1};
    }

    componentDidMount() {
      const timerId = setInterval(
          () => this.tick(),
          1000
       );
      this.setState({timerID:timerId});
    }

    componentWillUnmount() {
       clearInterval(this.state.timerID);
    }
  
    tick(){
        this.setState({currentDate: new Date()});
    }
  
    render() {
        return <div style={widgetStyle}>
            <span>Clock Widget:</span>
            <br/>
            <span>Current Local Time: {this.state.currentDate.toLocaleTimeString()}</span>
            </div>
    }
}