import * as React from "react"
import { connect } from 'react-redux'
import { StoreState } from './reducers'
import { ReduxAction } from './reducers/action'

export interface IProps {actions?:ReduxAction[]};

class ReduxActionLog extends React.Component<IProps, undefined>  {
    render(){
        return <div>
            {  
            this.props.actions.map(action =>
                <div key={action.key}>{action.action}</div>
            )
            }
            </div>
    }
}

const mapStateToProps = (state:StoreState, props:IProps) => {
    return {actions: state.actions} as IProps;
}

const mapDispatchToProps = (dispatch:any, props:IProps) => {
    return {} as IProps;
}

const ReduxActionLogComponent = connect(mapStateToProps, mapDispatchToProps)(ReduxActionLog)

export default ReduxActionLogComponent