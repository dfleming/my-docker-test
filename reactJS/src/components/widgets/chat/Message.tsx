import * as React from "react";

export interface IProps {text: string};

export class MessageComponent extends React.Component<IProps, undefined>  {

  constructor(props:IProps){
    super(props);
  }

    render(){
        return <div>Message: {this.props.text}</div>
    }
}