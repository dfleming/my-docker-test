import {Action} from '../reducers/messages'
import {ADD_MESSAGE} from '../constants'
let nextMessageId = 0;
export const addMessage = (text: string) => {
    return {
        type: ADD_MESSAGE,
        id: nextMessageId++,
        text: text
    } as Action
}