import { combineReducers } from 'redux'
import messages, {Message} from './Messages'
import actions, {ReduxAction} from './action'

export interface StoreState{messages: Message[], actions: ReduxAction[]};

const messageApp = combineReducers({messages, actions});
export default messageApp;