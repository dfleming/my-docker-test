
let actionId = 0;

export interface ReduxAction{key: number, action: string};

const action = (actionObj: any):ReduxAction => {
    return {
        key: actionId++,
        action: JSON.stringify(actionObj)
    } as ReduxAction
}

const actions = (state:ReduxAction[]=[], actionObj: any) => {
    
    return [...state, action(actionObj)];
}

export default actions;