
import {ADD_MESSAGE} from '../constants'

export interface Message{id: number, text: string};
export interface Action{id: number, type: string, text: string};

const message = (state:Message, action: Action):Message => {
    switch(action.type){
        case ADD_MESSAGE:
            return {
                id: action.id,
                text: action.text
            }
    }
}

const messages = (state:Message[]=[], action: Action) => {
    switch(action.type){
        case ADD_MESSAGE:
        return [
            ...state,
            message(undefined, action)
        ]
    }
    return state;
}

export default messages;