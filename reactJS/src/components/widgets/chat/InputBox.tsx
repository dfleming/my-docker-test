import * as React from "react";
import { connect } from 'react-redux'
import {addMessage} from './actions'
import {StoreState} from './reducers'

export interface IProps {sendMessage?:(message:string)=>void};
export interface IState {value: string, sendMessage:(message:string)=>void};

class InputBox extends React.Component<IProps, IState>  {
    
  constructor(props:IProps){
    super(props);
    this.state={value: 'Enter stuff here', sendMessage: props.sendMessage || (()=>{})};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event:any) {
    event.preventDefault();
    this.state.sendMessage(this.state.value);
    this.setState({value: ''});
  }
  handleChange(event:any) {
    this.setState({value: event.target.value});
  }

    render(){
        return (      
        <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <textarea value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
        );
    }
}

  const mapStateToProps = (state:StoreState, props:IProps) => {
      return {} as IProps;
  }
  
  const mapDispatchToProps = (dispatch:any, props:IProps) => {
    return {
        sendMessage: (message:string) => dispatch(addMessage(message))
    } as IProps;
  }

const InputBoxComponent = connect(mapStateToProps, mapDispatchToProps)(InputBox)

export default InputBoxComponent