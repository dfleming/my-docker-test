import * as React from "react";

import MessageBoxComponent from './MessageBox'
import InputBoxComponent from './InputBox';
import ReduxActionLogComponent from './ReduxActionLogWidget'
import { createStore, Store } from 'redux'
import messageApp from './reducers'
import { Provider } from 'react-redux'

import {widgetStyle} from '../../../style'

export interface IProps {};
export interface IState {store: Store<{}>};

export class ChatWidget extends React.Component<IProps, IState>  {

  

  constructor(props:IProps){
    super(props);
    let reduxStore = createStore(messageApp);
    this.state = {store: reduxStore};
  }

    render(){
        return (
        <Provider store={this.state.store}>
          <div style={widgetStyle}>
            <span>Redux Widgets:</span>
            <br/>
          <div style={widgetStyle}>
            <span>Redux Chat Widget:</span>
            <br/>
            <InputBoxComponent />
            <MessageBoxComponent/>
          </div>
          <div style={widgetStyle}>
            <span>Redux Action Log:</span>
            <br/>
            <ReduxActionLogComponent/>
          </div>
          </div>
        </Provider>
        );
    }
}