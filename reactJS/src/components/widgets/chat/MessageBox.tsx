import * as React from "react";
import { connect } from 'react-redux'
import {MessageComponent} from './Message';
import {StoreState} from './reducers';
import {Message} from './reducers/Messages'
export interface IProps {messages?:Message[]};

class MessageBox extends React.Component<IProps, undefined>  {

  constructor(props:IProps){
    super(props);
  }

    render(){
        return <div>
            {  
            this.props.messages.map(message =>
                <MessageComponent {...message} key={message.id}/>
            )
            }
            </div>
    }
}

  const mapStateToProps = (state:StoreState, props:IProps) => {
      return {messages: state.messages} as IProps;
  }
  
  const mapDispatchToProps = (dispatch:any, props:IProps) => {
      return {} as IProps;
  }

const MessageBoxComponent = connect(mapStateToProps, mapDispatchToProps)(MessageBox)

export default MessageBoxComponent