import * as React from "react";
import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';
import * as common from './../../common/Common';
const Util = common.Util;
import {widgetStyle} from '../../style'
export interface IProps {};
export interface IState {status:string, url:string};

export class NodeJSWidget extends React.Component<IProps, IState>  {
  constructor(props:IProps){
    super(props);
    this.state={status:"Loading", url:Util.getBaseURL()};
  }

  componentDidMount() {
    this.testConnection();
  }

  testConnection(){
    const me = this;
    axios.get(this.state.url)
    .then(function(response){
      me.handleConnectionResponse(response.data);
    })
    .catch(function(error){
      me.handleConnectionResponse('error occured!');
      console.error(error);
    })
  }

  handleConnectionResponse(response: string){
    this.setState({status:response});
  }

  render(){
    return <div style={widgetStyle}>
      <span>NodeJSWidget:</span>
      <br/>
      <span>{this.state.url} responded: {this.state.status}</span>
    </div>;
  }
}
