import * as React from "react";
import axios from 'axios';
import * as common from './../../common/Common';
import {widgetStyle} from '../../style'
const Util = common.Util;

export interface IProps {newsItems?:NewsItem[]};
export interface IState {newsItems?:NewsItem[], enabled: boolean, source?: string};

export interface MediaItem {height: number, width: number, url: string};
export interface NewsItem {title:string, link:string, media:MediaItem[]};

export class NewsWidget extends React.Component<IProps, IState> {

  constructor(props:IProps){
    super(props);
    this.handleNewsResponse = this.handleNewsResponse.bind(this);
    this.resolveItems = this.resolveItems.bind(this);
    this.onEnableWidget = this.onEnableWidget.bind(this);
    this.state = {newsItems:props.newsItems||[], enabled:false};
  }

  getNews(){
    const me = this;
    
    axios.get(Util.getBaseURL() + '/news/fetch')
    .then(function(response){
      me.handleNewsResponse(response);
    })
    .catch(function(error){
      console.error(error);
    })
  }

  handleNewsResponse(response:any){
      var parser = new DOMParser();
      var xmlDocument = parser.parseFromString(response.data, "text/xml");
      var source = this.resolveSource(xmlDocument);
      var result = this.resolveItems(xmlDocument);
      this.setState({newsItems:result, source: source});
  }
  resolveSource(xmlDocument:Document){
      var source = 'http://search.yahoo.com/mrss/';
      return source;
  }

  resolveItems(xmlDocument:Document){
      var result:NewsItem[] = [];
      var itemIterator = xmlDocument.evaluate('//rss//channel//item', xmlDocument, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
      var itemNode = itemIterator.iterateNext();
      while(itemNode){
          var title = xmlDocument.evaluate('title', itemNode, null, XPathResult.STRING_TYPE, null).stringValue;
          var link = xmlDocument.evaluate('link', itemNode, null, XPathResult.STRING_TYPE, null).stringValue;
          var mediaItems = this.resolveMedia(xmlDocument, itemNode);
          var newsItem:NewsItem = {title:title, link:link, media:mediaItems};
          result.push(newsItem);
          itemNode = itemIterator.iterateNext();
      }
      return result;
  }

  resolveMedia(xmlDocument:Document, itemNode: Node){
    var media:MediaItem[] = [];
    var nsResolver = xmlDocument.createNSResolver(itemNode);
    var contentIterator = xmlDocument.evaluate('media:content', itemNode, nsResolver, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
    var contentNode = contentIterator.iterateNext();
    while(contentNode){
        var mediaResolver = xmlDocument.createNSResolver(contentNode);
        var url = xmlDocument.evaluate('@url', contentNode, nsResolver, XPathResult.STRING_TYPE, null).stringValue;
        var height = xmlDocument.evaluate('@height', contentNode, nsResolver, XPathResult.NUMBER_TYPE, null).numberValue;
        var width = xmlDocument.evaluate('@width', contentNode, nsResolver, XPathResult.NUMBER_TYPE, null).numberValue;
        var mediaItem:MediaItem = {url, height, width};
        media.push(mediaItem);
        contentNode = contentIterator.iterateNext();
    }
    return media;
  }

  onEnableWidget(){
    this.getNews();
    this.setState({enabled:true});
  }

  render() {
      if(!this.state.enabled){
        return <div style={widgetStyle}>
          <span>NewsWidget:</span>
          <br /><input type="submit" value="Load News" onClick={this.onEnableWidget}/>
          </div>;
      }
      return <div style={widgetStyle}>
          <span>NewsWidget: {this.state.source}</span>
          <br />
          {
              this.state.newsItems.map(newsItem =>
                  <div key={newsItem.link} style={{ display: 'inline-block' }}>
                      <a href={newsItem.link} target="_blank">
                          <div title={newsItem.title} style={
                              {
                                  backgroundImage: 'url(' + newsItem.media[0].url + ')',
                                  height: newsItem.media[0].height,
                                  width: newsItem.media[0].width
                              }
                          }>
                          </div>
                      </a>
                  </div>
              )
          }
      </div>;
  }
}