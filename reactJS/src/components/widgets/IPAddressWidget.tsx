import * as React from "react";
import axios from 'axios';
import * as common from './../../common/Common';
const Util = common.Util;

import {widgetStyle} from '../../style'

export interface IProps {};
export interface IState {ip:string,'x-forwarded-for':string};

export class IPAddressWidget extends React.Component<IProps, IState>  {
  constructor(props:IProps){
    super(props);
    this.state={ip:"Loading", 'x-forwarded-for':"Loading"};
    this.getIPAddress = this.getIPAddress.bind(this);

  }


  getIPAddress(){
    const me = this;
    
    axios.get(Util.getBaseURL() + '/util/ip')
    .then(function(response){
      me.handleIPChange(response.data.ip, response.data['x-forwarded-for']);
    })
    .catch(function(error){
      me.handleIPChange('error occured!', 'error occured!');
      console.error(error);
    })
  }

  componentDidMount() {
    this.getIPAddress();
  }

  handleIPChange(ipString:string, forwardedFor:string){
    this.setState({ip:ipString, 'x-forwarded-for':forwardedFor});
  }

  render(){
    const myIp = this.state.ip;
    return <div style={widgetStyle}>
    <span>IPAddressWidget:</span>
    <br/>
    <span>IP value: {this.state.ip}</span>
    <br/>
    <span>'x-forwarded-for': {this.state['x-forwarded-for']}</span>
    </div>;
  }
}
