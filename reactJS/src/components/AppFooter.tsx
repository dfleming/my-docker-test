import * as React from "react";

import { Clock } from "./Clock";

export interface IProps {dateTime: Date};
export interface IState {};

export class AppFooter extends React.Component<IProps, IState> {
    render() {
        return <footer>
          <Clock/>
          <p>Source code at <a href="https://gitlab.com/dfleming/my-docker-test">https://gitlab.com/dfleming/my-docker-test</a></p>
          </footer>;
    }
}