export const widgetStyle = {
   borderStyle:'inset',
   borderRadius:'5px 10px',
   margin: '3px 3px 3px 3px',
   padding: '10px 10px 10px 10px',
   display: 'inline-block',
   background: 'linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%)' /* Standard syntax */
};