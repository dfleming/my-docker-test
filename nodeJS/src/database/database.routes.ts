"use strict";
import * as express from 'express';

import * as mongodb from 'mongodb';
const MongoClient = mongodb.MongoClient;

export function Router(){
  let router = express.Router();
  
  router.get("/test",function(req, res){
    
    let connectUrl = "mongodb://db:27017/test";
    let status = "Connecting...";
    MongoClient.connect(connectUrl, function(err, db){
    if(err!=null){
      console.log("Failed to connect to server");
      status = err + "\n";
    }else{
      console.log("Successfully connected to server");
      status = "Successfully connected to MONGO\n";
      db.close();
    }
      res.send(status)
    });
  });
  
  return router;
};