"use strict";
import * as express from 'express';
import axios from 'axios';

export function Router(){
  let router = express.Router();
  router.get("/fetch",function(req, res){
    axios.get('https://www.yahoo.com/news/rss/tech')
    .then(function(response){

      res.send(response.data);
    })
    .catch(function(error){
      console.log(error);
    })
  });
  return router;
};