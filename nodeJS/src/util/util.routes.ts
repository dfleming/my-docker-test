"use strict";
import * as express from 'express';

import * as mongodb from 'mongodb';
const MongoClient = mongodb.MongoClient;

export function Router(){
  let router = express.Router();

  router.get("/ip",function(req, res){
    res.send(
      {
        'ip':req.headers['x-originating-ip'],
        'x-forwarded-for':req.headers['x-forwarded-for-ip'],
        remoteAddress:req.connection.remoteAddress
      }
      );
  });

  router.get("/headers",function(req, res){
    res.send(
      req.headers
      );
  });

  return router;
};
