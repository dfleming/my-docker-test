"use strict";

import * as express from 'express';
import * as database from './src/database/database.routes'
import * as util from './src/util/util.routes'
import * as news from './src/news/news.routes'

const app = express();
const port = 8000;

import * as cors from 'cors';

//enable ALL cors requests
app.use(cors());

app.listen(port, function(){
  console.log('Server running on port: ' + port + '/');
})

app.get("/",function(req, res){
  res.send("hello world!")
});

app.use("/db",database.Router());
app.use("/util",util.Router());
app.use("/news",news.Router());