# JMeter in Docker


## About

Compose is used to create the master image and start-up a slave instance.

Ideally you would want to create the images and refer to them from an image repo.

A new master container is executed per run.

## Shortcut

docker-compose up -d

docker run -it --network=jmeter_back-tier --entrypoint /bin/bash jmeter_master -c "jmeter -n -t SampleScript.jmx -Rslave:10999"